/**
 * Created by: John Yun
 * Class: CS49J
 * Date Created: 9/21/2021
 */

import java.util.Scanner;

public class E4_23 {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Enter Soda Can's height and diameter, use space to separate elements: ");
        double dHeight, dDiameter;
        dHeight = keyboard.nextDouble();
        dDiameter = keyboard.nextDouble();;
        SodaCan coke = new SodaCan(dHeight, dDiameter);
        System.out.printf("Soda Can Volume: %.2f \n", coke.getVolume());
        System.out.printf("Soda Can Area: %.2f", coke.getSurfaceArea());
    }
}
/***********************************************************************
 * Sample Output of Main:
 *
 * Enter Soda Can's height and diameter, use space to separate elements:
 * 5.0 3.0
 * Soda Can Volume: 35.34
 * Soda Can Area: 61.26
 * Process finished with exit code 0
 */

