/**
 * This is the soda can class
 *
 */

import java.lang.Math;
public class SodaCan {
    private double dHeight;
    private double dDiameter;

    /**
     * This is the constructor method with parameters
     * for the SodaCan Class
     * @param dHcm
     * @param dDcm
     * @return nothing
     */
    public SodaCan(double dHcm, double dDcm){
        dHeight = dHcm;
        dDiameter = dDcm;
    }

//Getter methods
    /**
     * This the getter method for height
     * @param nothing
     * @return the double Height of the can
     */
    public double getHeight(){
        return dHeight;
    }

    /**
     * Gets the Diameter of the object
     * @param nothing
     * @return the diameter of the can
     */
    public double getDiameter() {
        return dDiameter;
    }

//Setter methods
    public void setdHeight(double height){
        dHeight = height;
    }

    public void setDiameter(double diameter) {
        dDiameter = diameter;
    }

    //Implementation methods

    /**
     * Calculates the volume of the can:
     *    PI * r^2 * height
     * @param nothing
     * @return the double value can's volume.
     */
    public double getVolume(){
        double dTopArea = Math.pow(dDiameter/2, 2) * Math.PI;
        double dVolume = dTopArea * dHeight;
        return dVolume;
    }

    /**
     * Calculates and returns the SurfaceArea of the can
     * 2PI * r * h + 2 PI * r^2  ---> r is D/2
     * @param nothing
     * @return the surface area of the can
     */
    public double getSurfaceArea(){
        double dArea;
        double dTopArea = Math.pow(dDiameter/2, 2) * Math.PI * 2;
        double dMiddleArea = Math.PI * dDiameter * dHeight;
        dArea = dTopArea + dMiddleArea;
        return dArea;
    }
}
